#!/usr/bin/env python3

import requests
import sys
import xml.etree.ElementTree as ET


if __name__ == '__main__':

    root = ET.parse(sys.argv[1])
    user = root.find('calendar/id').text
    key = root.find('calendar/key').text
    url = 'https://www.googleapis.com/calendar/v3/calendars/%s/events?key=%s' % (user, key,)
    j = requests.get(url).json()
    assert j['kind'] == 'calendar#events'
