<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
  <xsl:output method="html"/>
  <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>
    <html lang="en">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"/>
        <link rel="stylesheet" href="css/mytheme.min.css" integrity=""/>
        <link rel="shortcut icon" type="image/png" href="apple-touch-icon.png?"/>
        <title><xsl:value-of select="site/title"/></title>
      </head>
      
      <body>
        <img src="images/banner.jpg" class="img-fluid"/>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">
            <img src="images/bsa-color-logo.png" alt="BSA" class="navbar-logo"/>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <xsl:for-each select="site/sections/section">
                <li class="nav-item">
                  <a class="nav-link" href="#{id}"><xsl:value-of select="title"/></a>
                </li>
              </xsl:for-each>
            </ul>
          </div>
        </nav>

        <h1><xsl:value-of select="site/title"/></h1>
        
        <xsl:for-each select="site/sections/section">
          <a id="{id}">
            <div class="card">
              <div class="card-body">
                <h2 class="card-title"><xsl:value-of select="title"/></h2>
                <xsl:variable name="fn" select="source"/>
                <xsl:value-of select="unparsed-text($fn)" disable-output-escaping="yes"/>
              </div>
            </div>
          </a>
        </xsl:for-each>
        
        <footer class="footer bg-light">
          <div class="container-fluid">
            <div class="row align-items-center">
              <div id="logo-footer" class="col-md-1">
                <img src="apple-touch-icon.png"/>
              </div>
              <div class="col-md-4 text-left">
                <address>
                  <strong><xsl:value-of select="site/contact/name"/></strong><br/>
                  <xsl:variable name="email" select="site/contact/email"/>
                  <a href="mailto:{$email}"><xsl:value-of select="$email"/></a>
                </address>
              </div>
              <div class="col-md-5 text-left">
                <ul class="list-inline">
                  <li class="list-inline-item"><a href="https://spcah.org">Southminster Presbyterian Church</a><br/></li>
                  <li class="list-inline-item"><a href="https://pathwaytoadventure.org/districts/fivecreeks/">Five Creeks District</a><br/></li>
                  <li class="list-inline-item"><a href="https://www.pathwaytoadventure.org">Pathway to Adventure Council</a><br/></li>
                  <li class="list-inline-item"><a href="https://www.scouting.org">Boy Scouts of America</a><br/></li>
                </ul>
              </div>
              <div class="col-md-2 text-left">
                <ul class="list-inline">
		  <li class="list-inline-item"><a href="https://www.facebook.com/cubscoutpack129ah"><i class="fab fa-facebook fa-2x text-dark"></i></a></li>
                  <li class="list-inline-item"><a href="https://gitlab.com/cubscoutpack129ah"><i class="fab fa-gitlab fa-2x text-dark"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="js/format-google-calendar.min.js"></script>
        <script src="js/load-calendar-events.js"></script>
        <script type="text/javascript">loadCalendarEvents({daysPast: <xsl:value-of select="site/calendar/days/past"/>, daysUpcoming:<xsl:value-of select="site/calendar/days/upcoming"/>, calendarUrl:'https://www.googleapis.com/calendar/v3/calendars/<xsl:value-of select="site/calendar/id"/>/events?key=<xsl:value-of select="site/calendar/key"/>'});</script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
