#!/usr/bin/env python3

from bs4 import BeautifulSoup
import re
import requests
import sys
import time

# If we don't use a "real" browser, www.scouting.org return 403.
HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15'}


def check_link(u):

    result = [u, None]
    for attempt in range(3):
        try:
            r = requests.get(u, headers=HEADERS, timeout=10)
            result[1] = r.status_code
            if result[1] == 200:
                break
        except Exception as err:
            pass
        time.sleep(0.25)
    return result


def check_links_in_file(fn):

    soup = BeautifulSoup(open(fn), features='html5lib')
    for link in soup.find_all('a', href=True):
        if not re.search('^http(s)?://', link['href']):
            continue
        yield check_link(link['href'])
        time.sleep(0.25)


if __name__ == '__main__':

    broken = 0
    for fn in sys.argv[1:]:
        for result in check_links_in_file(fn):
            print(result)
            if result[1] not in (200,):
                broken += 1
    sys.exit(0 if broken == 0 else 1)

