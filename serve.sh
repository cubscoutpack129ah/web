#!/bin/bash

set -e

PORT=8001

ORIGIN="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$ORIGIN"/public
python -m http.server "$PORT" 
