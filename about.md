Pack 129 is located in Arlington Heights, IL.  It is chartered by
[Pathway to Adventure Council](http://www.pathwaytoadventure.org) and
[Southminster Presbyterian Church](https://spcah.org).  Pack events
are often held at [Dryden Elementary](http://www.sd25.org/dryden).