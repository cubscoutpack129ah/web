# What is this?

This directory contains the [static site
generator](https://fvsch.com/static-site-generators/) and content used
to build the web site for Cub Scout Pack 129 in Arlington Heights, IL.
The design is original, but it follows [BSA
guidelines](https://scoutingwire.org/bsa-brand-center/brand-identity/)
on font selection, palette, and logo use where practical.

# What is required to use this?

## Local development

* [Node.js](https://nodejs.org/en/download/)
* [npm](https://www.npmjs.com) packages
    * [gulp-cli](https://www.npmjs.com/package/gulp-cli)
    * [showdown](https://www.npmjs.com/package/showdown)
* [Saxon-HE](http://saxon.sourceforge.net/#F9.8HE)
* [GNU Make](https://www.gnu.org/software/make/)
* [Git](https://git-scm.com) (to push changes to [GitLab](https://about.gitlab.com))
* [Python 3](https://www.python.org/downloads/)
    * [bs4](https://pypi.org/project/beautifulsoup4/)
    * [html5lib](https://pypi.org/project/html5lib/)
    * [requests](https://pypi.org/project/requests/)

With [Node.js](https://nodejs.org/en/download/),
[npm](https://www.npmjs.com),
[Python 3](https://www.python.org/downloads/), and
[Saxon-HE](http://saxon.sourceforge.net/#F9.8HE) (XSLT Version 2
support is required, so xsltproc will not work) installed, from this
directory, run:

    pip3 install bs4 html5lib requests
    npm install -g gulp-cli@2.2.1
    npm install -g showdown@1.9.1
    npm install
    make

To test the site, from this directory, run:

    ./serve.sh

And browse to `http://localhost:8001/`.

## Update on GitLab

The site is deployed by a GitLab CI/CD pipeline, so it can be updated
by merging changes to this repository.  At most you'll need a text editor and git client, but it's also possible to edit text directly on GitLab.

# How do I make changes?

Arbitrary changes are potentially difficult, but anticipated changes
are easy.

## Update the calendar

Don't.  It's populated by client browsers directly from a Google
Calendar.  NB there is a
[problem](https://github.com/MilanLund/FormatGoogleCalendar/issues/12)
with the JavaScript to format calendar events which causes all-day
events to be presented one day too early (at least for those of us
west of GMT.)

## Update an existing section

Most sections are defined by a Markdown (.md) file.  Edit the
appropriate Markdown file.  See the [Markdown:
Syntax](https://daringfireball.net/projects/markdown/syntax) reference
for help.

## Add a new section

Besides creating a new Markdown file (which we'll call `example.md` for
illustrative purposes), you'll need to add a new section element to
`site.xml`.  Note that in each section element, the source is a HTML
file.  In our example, the new section would look like this:

    <section>
      <title>Example</title>
      <id>example</id>
      <source>example.html</source>
    </section>

After creating the new Markdown file and adding its section to
`site.xml`, update `Makefile` so that the list of generated HTML files
includes the source we need to generate.  Following with our example:

    GENERATED_HTML = about.html awards.html join.html example.html

The build process will create the new file `example.html` from
`example.md` and insert its contents into the single rendered page.

# Third-party components

## FormatGoogleCalendar

Google calendars are incorporated into the rendered page with
[FormatGoogleCalendar](https://github.com/MilanLund/FormatGoogleCalendar).

## Bootstrap

[Bootstrap 4](https://getbootstrap.com/docs/4.1/getting-started/download/)

## Bootstrap Theme Kit

This site was created by cloning [Bootstrap Theme
Kit](https://github.com/HackerThemes/theme-kit), customizing SCSS
files, and adding additional content.
