STATIC_HTML = events.html
GENERATED_HTML = about.html awards.html join.html
CSS_FILES = css/mytheme.css css/mytheme.min.css
SCSS_FILES = scss/_custom-styles.scss scss/_custom-variables.scss scss/mytheme.scss

.PHONY : all
all : public/index.html \
      public/files \
      public/images \
      public/js \
      public/css \
      public/apple-touch-icon.png \
      test

public/index.html : public index.xsl site.xml $(STATIC_HTML) $(GENERATED_HTML)
	saxonb-xslt -s:site.xml -xsl:index.xsl -o:$@

%.html : %.md
	showdown makehtml -i $< -o $@

public/% : % public
	rsync -a --delete $< public

public/css : $(CSS_FILES)
	rsync -a --delete css public

$(CSS_FILES) : $(SCSS_FILES)
	gulp

public :
	mkdir public

.PHONY : test
test : check_links check_calendar

.PHONY : check_links
check_links : public/index.html
	./check_links.py public/index.html

.PHONY : check_calendar
check_calendar :
	./check_calendar.py site.xml

.PHONY : clean
clean :
	rm -rf $(GENERATED_HTML) css public
