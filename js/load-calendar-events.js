function loadCalendarEvents(obj) {
    var ms = Date.now();
    var ms_min = ms - (obj.daysPast * 86400000);
    var ms_max = ms + (obj.daysUpcoming * 86400000);
    var dmin = (new Date(ms_min)).toISOString();
    var dmax = (new Date(ms_max)).toISOString();
    
    formatGoogleCalendar.init({calendarUrl: obj.calendarUrl,
                               past:true,
                               upcoming:true,
                               pastTopN: 10,
                               upcomingTopN: 30,
                               recurringEvents: true,
                               upcomingHeading: '<h3>Upcoming</h3>',
                               pastHeading: '<h3>Past</h3>',
                               format: ['<div class="event-summary mt-3">', '*summary*', '</div><div class="event-date">', '*date*', '</div><div class="event-description">', '*description*', '</div><div class="event-location">', '*location*', '</div>'],
                               timeMin: dmin,
                               timeMax: dmax});
}
